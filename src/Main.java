
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import domain.Address;
import domain.Permission;
import domain.Person;
import domain.Role;
import domain.User;
import service.UserService;


public class Main {
	private static List<User> users;

	private static void init() {
		
		Person mateusz = new Person();
		Person adrian = new Person();
		Person stefan = new Person();
		
		mateusz.setAge(21);
		mateusz.setName("Mateusz");
		mateusz.setSurname("Garboski");
		
		adrian.setAge(16);
		adrian.setName("adrian");
		adrian.setSurname("szpak");
		
		stefan.setAge(52);
		stefan.setName("stefan");
		stefan.setSurname("wysocki");
		
		
		
		User user1 = new User("mateusz", "abc1", mateusz);
		User user2 = new User("rozgadany1", "passwd1", adrian);
		User user3 = new User("batory", "wojna", stefan);
		
		
		users = Arrays.asList(user1, user2, user3);
		
		Role roleUser = new Role();
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		mateusz.setRole(roleAdmin);
		stefan.setRole(roleGuest);
		adrian.setRole(roleUser);
		
		Address a1 = new Address("Poland", "ilawa", "okulickiego", 1);
		Address a2 = new Address("Poland", "ilawa", "wyzwolenia", 2);
		Address a3 = new Address("Poland", "ilawa", "krotka", 3);
		
		mateusz.setAddresses(Arrays.asList(a1,a2,a3));
		stefan.setAddresses(Arrays.asList(a1));
		adrian.setAddresses(Arrays.asList(a2));
		
		Permission permWrite = new Permission();
		Permission permRead = new Permission();
		Permission permExecute = new Permission();
		
		permWrite.setName("write");
		permRead.setName("read");
		permExecute.setName("execute");
		
		List<Permission> permUser = Arrays.asList(permWrite,permRead);
		List<Permission> permGuest = Arrays.asList(permRead);
		List<Permission> permAdmin = Arrays.asList(permWrite,permRead,permExecute);
		
		roleUser.setPermissions(permUser);
		roleGuest.setPermissions(permGuest);
		roleAdmin.setPermissions(permAdmin);
		List<User> moreThan2Addresses = UserService.findUsersWhoHaveMoreThanOneAddress(users);
		System.out.println(moreThan2Addresses);
		
		Person oldest = UserService.findOldestPerson(users);
		System.out.println("Najstarszy jest: " + oldest.getName() + " " 
							+ oldest.getSurname() + " lat " + oldest.getAge());
		User longest = UserService.findUserWithLongestUsername(users);
		System.out.println("Najdluzszy login ma: " + longest.getName());
		
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		System.out.println("Ukonczone 18 lat ma:" + result);
		
		List<String> nameStartingWithA = UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users);
		System.out.println("Posortowana lista uprawnien uzytkownikow z imieniem na A: ");
		System.out.println(nameStartingWithA);

		System.out.println("Lista uprawnien uzytkownikow z nazwiskiem na S: ");
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
		
		Map<Role, List<User>> groupUsersByRole = UserService.groupUsersByRole(users);
		System.out.println(groupUsersByRole);
		
		Map<Boolean, List<User>> groupUsersBy18 = UserService.partitionUserByUnderAndOver18(users);
		System.out.println(groupUsersBy18);
	
	}
	


	public static void main(String[] args) {
		init();
	
	}
}
